# Mymoria Technical Task
___

It's a Multi-step web application which allows users to enter data and register. This whole application is developed in Javascript, ES6 
using the React and Redux libraries. User experience navigating through multi-step application although it's a Single Page Application.

Output can be found in [wiki of this repository](https://bitbucket.org/vjvijayg/mymoria_technical-task/wiki/Home).

### Installation and Setup
___

- Download and install [Node.js](https://nodejs.org/en/)
- clone or download this [repository](https://bitbucket.org/vjvijayg/mymoria_technical-task)
- Navigate to the project directory then install npm package manager using the commands `npm init`
- Next command `npm install --save-dev webpack webpack-dev-server html-webpack-plugin` to install webpack
- Next command `npm install --save-dev babel-core babel-loader` to install bable
- Execute `npm install --save react` to install [react](https://facebook.github.io/react/docs/installation.html)
- Execute next command `npm install --save react-dom` to install react-dom
- Next command `npm install redux` to install [redux](https://www.npmjs.com/package/redux)
- Next command `npm install --save redux-form` to install [redux-form](https://www.npmjs.com/package/redux-form)
- Next command `npm install --save react-redux` to install [react-redux](https://www.npmjs.com/package/react-redux)
- Next command `npm i redux-form-website-template` to install redux-form-website-template
- Next command `npm i react-datepicker` to install react-datepicker
- Next command `npm install moment` to install moment
- The last command `npm start` to deploy the app on [local host](http://localhost:3000/)

### Dependencies
___

- npm Packages
- babel
- webpack
- react
- react-dom
- react-redux
- redux
- redux-form
- redux-form-website-template
- react-datepicker
- moment